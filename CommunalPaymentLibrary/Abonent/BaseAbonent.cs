﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunalPaymentLibrary
{
    public abstract class BaseAbonent
    {
        [LocalAbonentDataMapperAttribute(MappingName = "Abonent")]
        public string AbonentNumber { get; set; }
        public abstract BodCommunalService CommunalServiceId { get; }
        public abstract decimal Debt { get; }
    }    
}
