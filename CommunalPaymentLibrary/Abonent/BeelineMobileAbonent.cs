﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public class BeelineMobileAbonent : BaseLocalAbonent
    {
        [LocalAbonentDataMapperAttribute(MappingName = "CommunalServiceId", ServiceID = 52)]
        public override BodCommunalService CommunalServiceId { get { return BodCommunalService.BeelineMobile; } }

        public override decimal Debt
        {
            get { return Amount; }
        }

        [LocalAbonentDataMapperAttribute(MappingName = "4", ID = 4, ServiceID = 52, ParamID = 9, FieldNameSource = "salin", DescriptionArm = "Պարտք")]
        public decimal Amount { get; set; }

    }
}
