﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{

    public abstract class BaseLocalAbonent : BaseAbonent
    {
        [LocalAbonentDataMapperAttribute(MappingName = "AbonentId")]
        public int AbonentId { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "AbonentName")]
        public string FullName { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "Address")]
        public string FullAddress { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "RegionName")]
        public string ServiceRegionName { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "BranchName")]
        public string ServiceBranchName { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "ServiceBranchCode")]
        public string ServiceBranchCode { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "Phone")]
        public string Phone { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "DateOf")]
        public DateTime DateOf { get; set; }

        [LocalAbonentDataMapperAttribute(MappingName = "ProviderAbonentId")]
        public string ProviderAbonentId { get; set; }
    }
}
