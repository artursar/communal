﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public abstract class BaseWaterAbonent : BaseLocalAbonent
    {

    }

    public class WaterCitizen : BaseWaterAbonent
    {
        [LocalAbonentDataMapperAttribute(MappingName = "CommunalServiceId", ServiceID = 59)]
        public override BodCommunalService CommunalServiceId { get { return BodCommunalService.WaterCitizen; } }

        public override decimal Debt { get { return WaterDebt; } }

        /// <summary>
        /// Պարտք
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "64", ID = 64, ParamID = 26, FieldNameSource = "vsaldo", DescriptionArm = "Պարտք")]
        public decimal WaterDebt { get; set; }

        /// <summary>
        /// Գումար
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "68", ID = 68, ParamID = 27, FieldNameSource = "calculate", DescriptionArm = "Գումար")]
        public decimal WaterCost { get; set; }

        /// <summary>
        /// Ծախս
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "67", ID = 67, ParamID = 28, FieldNameSource = "m3", DescriptionArm = "Ծախս")]
        public decimal Consumption { get; set; }

        /// <summary>
        /// Վճարված է
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "66", ID = 66, ParamID = 29, FieldNameSource = "nax_paid", DescriptionArm = "Վճարված է")]
        public decimal PayedAmount { get; set; }

        /// <summary>
        /// Նախավճար ամսվա սկզբին
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "65", ID = 65, ParamID = 30, FieldNameSource = "nax_vsaldo", DescriptionArm = "Նախավճար ամսվա սկզբին")]
        public decimal AdvancePaymentInTheStartOfMonth { get; set; }

        /// <summary>
        /// Լրացուցիչ հաշվիչ
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "69", ID = 69, ParamID = 31, FieldNameSource = "dop_cuc", DescriptionArm = "Լրացուցիչ հաշվիչ")]
        public decimal AdditionalMeter { get; set; }

        /// <summary>
        /// Հաշվիչների ցուցմունքները
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "71", ID = 71, ParamID = 33, FieldNameSource = "cucmunk", DescriptionArm = "Հաշվիչների ցուցմունքները")]
        public decimal MetersReadings { get; set; }

        /// <summary>
        /// Ջրամատակարարման ժամերը
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 59, MappingName = "70", ID = 70, ParamID = 58, FieldNameSource = "jam", DescriptionArm = "Ջրամատակարարման ժամերը")]
        public string WaterSupplyHours { get; set; }
    }

    public class WaterPublic : BaseWaterAbonent
    {
        [LocalAbonentDataMapperAttribute(MappingName = "CommunalServiceId", ServiceID = 60)]
        public override BodCommunalService CommunalServiceId { get { return BodCommunalService.WaterPublic; } }

        public override decimal Debt { get { return WaterDebt; } }

        /// <summary>
        /// Պարտք
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "400", ID = 400, ParamID = 26, FieldNameSource = "vsaldo", DescriptionArm = "Պարտք")]
        public decimal WaterDebt { get; set; }

        /// <summary>
        /// Գումար
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "404", ID = 404, ParamID = 27, FieldNameSource = "calculate", DescriptionArm = "Գումար")]
        public decimal WaterCost { get; set; }

        /// <summary>
        /// Ծախս
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "403", ID = 403, ParamID = 28, FieldNameSource = "m3", DescriptionArm = "Ծախս")]
        public decimal Consumption { get; set; }

        /// <summary>
        /// Վճարված է
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "402", ID = 402, ParamID = 29, FieldNameSource = "nax_paid", DescriptionArm = "Վճարված է")]
        public decimal PayedAmount { get; set; }

        /// <summary>
        /// Նախավճար ամսվա սկզբին
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "401", ID = 401, ParamID = 30, FieldNameSource = "nax_vsaldo", DescriptionArm = "Նախավճար ամսվա սկզբին")]
        public decimal AdvancePaymentInTheStartOfMonth { get; set; }

        /// <summary>
        /// Լրացուցիչ հաշվիչ
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "405", ID = 405, ParamID = 31, FieldNameSource = "dop_cuc", DescriptionArm = "Լրացուցիչ հաշվիչ")]
        public decimal AdditionalMeter { get; set; }

        /// <summary>
        /// Հաշվիչների ցուցմունքները
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "407", ID = 407, ParamID = 33, FieldNameSource = "cucmunk", DescriptionArm = "Հաշվիչների ցուցմունքները")]
        public decimal MetersReadings { get; set; }

        /// <summary>
        /// Ջրամատակարարման ժամերը
        /// </summary>
        [LocalAbonentDataMapperAttribute(ServiceID = 60, MappingName = "406", ID = 406, ParamID = 58, FieldNameSource = "jam", DescriptionArm = "Ջրամատակարարման ժամերը")]
        public string WaterSupplyHours { get; set; }
    }    
}
