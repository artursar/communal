﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunalPaymentLibrary
{
    public class ElectricityCitizen : BaseLocalAbonent
    {

        [LocalAbonentDataMapperAttribute(MappingName = "CommunalServiceId", ServiceID = 57)]
        public override BodCommunalService CommunalServiceId { get { return BodCommunalService.ElectricityCitizen; } }

        public override decimal Debt { get { return FinalBalanceOfDebt; } }

        /// <summary>
        /// Որոշիչ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "41", ID = 41, ServiceID = 57, ParamID = 48, FieldNameSource = "id", DescriptionArm = "Որոշիչ")]
        public string IdEnergy { get; set; }

        /// <summary>
        /// Ենթակայանի կոդը
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "20", ID = 20, ServiceID = 57, ParamID = 14, FieldNameSource = "s_cod", DescriptionArm = "Ենթակայանի կոդը")]
        public string SubstationCode { get; set; }

        /// <summary>
        /// Պարտք կամ նախավճար ամսվա սկզբին
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "21", ID = 21, ServiceID = 57, ParamID = 15, FieldNameSource = "start", DescriptionArm = "Պարտք ամսվա սկզբին")]
        public decimal DebtInTheStartOfMonth { get; set; }

        /// <summary>
        /// Ծախս (Ընդամենը) - կվտ.ժ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "22", ID = 22, ServiceID = 57, ParamID = 16, FieldNameSource = "rasx", DescriptionArm = "Ծախս")]
        public decimal Consumption { get; set; }

        /// <summary>
        /// էներգիայի արժեքը - դրամ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "23", ID = 23, ServiceID = 57, ParamID = 17, FieldNameSource = "rasxdram", DescriptionArm = "Գումար")]
        public decimal EnergyCost { get; set; }

        /// <summary>
        /// Վճարված է
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "24", ID = 24, ServiceID = 57, ParamID = 18, FieldNameSource = "plat", DescriptionArm = "Վճարված է")]
        public decimal PayedAmount { get; set; }

        /// <summary>
        /// Գիշերային գումարը 
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "26", ID = 26, ServiceID = 57, ParamID = 20, FieldNameSource = "dramgish", DescriptionArm = "Որից գիշերային գումարը")]
        public decimal NightCost { get; set; }

        /// <summary>
        /// Ցերեկային գումարը - դրամ
        /// </summary>
        public decimal DaylightCost { get { return EnergyCost - NightCost; } }

        /// <summary>
        /// Գիշերային ծախս - կվտ.ժ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "25", ID = 25, ServiceID = 57, ParamID = 19, FieldNameSource = "rasxgish", DescriptionArm = "Որից գիշերային ծախսը")]
        public decimal NightConsumption { get; set; }

        /// <summary>
        /// Ցերեկային ծախս - կվտ.ժ
        /// </summary>
        public decimal DaylightConsumption { get { return Consumption - NightConsumption; } }

        /// <summary>
        /// Ցերեկային  սակագին
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "35", ID = 35, ServiceID = 57, ParamID = 69, FieldNameSource = "tarif", DescriptionArm = "Ցերեկային  սակագին")]
        public decimal DaylightTariff { get; set; }

        /// <summary>
        /// Գիշերային սակագին
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "40", ID = 40, ServiceID = 57, ParamID = 24, FieldNameSource = "tarif", DescriptionArm = "Գիշերային  սակագին")]
        public decimal NightTariff { get; set; }

        /// <summary>
        /// Պայմանագրի կնքման ամսաթիվ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "27", ID = 27, ServiceID = 57, ParamID = 78, FieldNameSource = "paymdate", DescriptionArm = "Պայմանագրի կնքման ամսաթիվ")]
        public string AgreementDateStr { get; set; }

        /// <summary>
        /// ԷՄՕԿ 77 կետով հաշվարկված տույժ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "28", ID = 28, ServiceID = 57, ParamID = 79, FieldNameSource = "emok77_t", DescriptionArm = "ԷՄՕԿ 77-ով հաշվարկվ. տույժ")]
        public decimal PenaltyCalculatedByEmok77 { get; set; }

        /// <summary>
        /// ԷՄՕԿ 77 կետով տույժի եկամտահարկ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "29", ID = 29, ServiceID = 57, ParamID = 80, FieldNameSource = "emok77eh", DescriptionArm = "ԷՄՕԿ 77-ով տույժի եկամտահարկ")]
        public decimal PenaltyTaxByEmok77 { get; set; }
        /// <summary>
        /// ԷՄՕԿ 77 կետով վճարված տույժ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "30", ID = 30, ServiceID = 57, ParamID = 81, FieldNameSource = "emok77_p", DescriptionArm = "ԷՄՕԿ 77-ով վճարված տույժ")]
        public decimal PenaltyPayedByEmok77 { get; set; }
        
        /// <summary>
        /// Նախորդ ցուցմունքը (Ց)
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "32", ID = 32, ServiceID = 57, ParamID = 62, FieldNameSource = "n_cuc", DescriptionArm = "Նախորդ ցուցմունքը (Ց)")]
        public decimal PreviousMeterReading { get; set; }

        /// <summary>
        /// Պարտքի վերջնական մնացորդ
        /// </summary>
        [LocalAbonentDataMapperAttribute(MappingName = "303", ID = 303, ServiceID = 57, ParamID = 99, FieldNameSource = "a_end", DescriptionArm = "Պարտքի վերջնական մնացորդ")]
        public decimal FinalBalanceOfDebt { get; set; }

    }

}
