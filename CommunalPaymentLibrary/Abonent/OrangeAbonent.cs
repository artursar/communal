﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public class OrangeAbonent : BaseOnlineAbonent, IMobileAbonent
    {
        public override BodCommunalService CommunalServiceId
        {
            get { return BodCommunalService.Orange; }
        }

        public override decimal Debt
        {
            get { return Amount; }
        }


        // ParamID 150	Amount
        public decimal Amount { get; set; }
        // ParamID 151	AbonentName
        public string AbonentName { get; set; }
        // ParamID 152	Account
        public string Account { get; set; }
        // ParamID 153	PayMethod
        public int PayMethod { get; set; }

    }
}
