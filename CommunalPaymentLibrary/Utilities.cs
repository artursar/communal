﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommunalPaymentLibrary
{
    internal class Utilities
    {
        public static DbCredentials DbLive { get; set; }
        public static DbCredentials DbLocal { get; set; }
        public static DbCredentials DbTest { get; set; }

        public static string CommunalDataDateTimeFormat { get { return "yyyyMMdd"; } }

        public const string BEELINE = "BEELINE";
        public const string VIVACELL = "VIVACELL";
        public const string ORANGE = "ORANGE";
    }

    internal static class Extenstions
    {
        public static DateTime? ToCommunalDate(this string val)
        {
            try
            {
                return DateTime.ParseExact(val, Utilities.CommunalDataDateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class Settings
    {
        public PaymentSource Source { get; set; }
        public int Branch { get; set; }
        public bool IsEvening { get; set; }
        public int BodAccountId { get; set; }
        public bool IsLive { get; set; }
        public DbCredentials DbSettings { get; set; }
    }

    public class DbCredentials
    {
        public DbCredentials()
        {
        }

        public DbCredentials(string server, string dbName, string username, string password)
        {
            this.Server = server;
            this.DbName = dbName;
            this.Username = username;
            this.Password = password;
        }

        public string Server { get; set; }
        public string DbName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
