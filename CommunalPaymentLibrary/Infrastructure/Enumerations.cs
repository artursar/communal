﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public enum BodCommunalService
    {
        None = 0,
        /// <summary>
        /// Beeline Or HiLine - 51
        /// </summary>
        BeelineOrHiLine = 51,
        /// <summary>
        /// Beeline Mobile - 52
        /// </summary>
        BeelineMobile = 52,
        /// <summary>
        /// Orange - 55
        /// </summary>
        Orange = 55,
        /// <summary>
        /// VivaCell - 56
        /// </summary>
        VivaCell = 56,
        /// <summary>
        /// Էլ․ ցանց բնակիչ - 57
        /// </summary>
        ElectricityCitizen = 57,
        /// <summary>
        /// Էլ․ ցանց հանրային - 58
        /// </summary>
        ElectricityPublic = 58,
        /// <summary>
        /// Ջուր բնակիչ - 59
        /// </summary>
        WaterCitizen = 59,
        /// <summary>
        /// Ջուր հանրային - 60
        /// </summary>
        WaterPublic = 60,
        /// <summary>
        /// Հայջրմուղկոյուղի - 61
        /// </summary>
        ArmWater = 61,
        /// <summary>
        /// Գազ սպառման դիմաց - 62
        /// </summary>
        GasConsumptionCitizen = 62,
        /// <summary>
        /// Գազի սպասարկում - 63
        /// </summary>
        GasServiceCitizen = 63,
        /// <summary>
        /// Գազ հանրային - 64
        /// </summary>
        GasConsumptionPublic = 64,
        /// <summary>
        /// GNC RosTeleCom - 69
        /// </summary>
        GNC_RosTeleCom = 69,
        /// <summary>
        /// Ucom - 70
        /// </summary>
        UCom = 70,
        //SPB_TV = 54,
    }

    /// <summary>
    /// Վճարման աղբյուր
    /// </summary>
    public enum PaymentSource
    {
        /// <summary>
        /// Դրամարկղի մուծումներ
        /// </summary>
        FrontOffice = 1,
        /// <summary>
        /// "ՀԷԲ"-ի Վեբ էջից նախկինում կատարվող մուծումներ(այժմ չի կիրառվում)
        /// </summary>
        OldAEBWebPage = 2,
        /// <summary>
        /// Պայմանագրի հիման վրա կատարվող մուծումներ
        /// </summary>
        CommunalAgreements = 3,
        /// <summary>
        /// "Tandem" համակարգով կատարվող մուծումներ
        /// </summary>
        Tandem = 4,
        /// <summary>
        /// "TerminalBanking" համակարգով կատարվող մուծումներ
        /// </summary>
        TerminalBanking = 5,
        /// <summary>
        /// "RemoteBanking" համակարգով կատարվող մուծումներ
        /// </summary>
        RemoteBanking = 6
    }

}
