﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public class OnlineCommunalPartialPayedException : Exception
    {
        public int PayId { get; set; }

        public OnlineCommunalPartialPayedException()
        {
        }

        public OnlineCommunalPartialPayedException(string message)
            : base(message)
        {
        }

        public OnlineCommunalPartialPayedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected OnlineCommunalPartialPayedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
            {
                PayId = info.GetInt32("PayId");
            }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info != null)
            {
                info.AddValue("PayId", PayId);
            }
        }
    }
}
