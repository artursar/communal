﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public sealed class MoneyAmount : IEquatable<MoneyAmount>
    {
        public decimal Amount { get; private set; }
        public string CurrencySymbol { get; private set; }

        public MoneyAmount(decimal amount, string currencySymbol)
        {
            if (amount < 0)
                throw new ArgumentException("amount");

            this.Amount = amount;
            this.CurrencySymbol = currencySymbol;
        }

        public MoneyAmount Scale(decimal factor)
        {
            return new MoneyAmount(this.Amount * factor, this.CurrencySymbol);
        }

        public static MoneyAmount operator *(MoneyAmount amount, decimal factor)
        {
            return amount.Scale(factor);
        }

        // Additional checks needed
        public static MoneyAmount operator +(MoneyAmount a, MoneyAmount b)
        {
            return new MoneyAmount(a.Amount + b.Amount, a.CurrencySymbol);
        }

        public static bool operator ==(MoneyAmount a, MoneyAmount b)
        {
            return (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null))
                || (!object.ReferenceEquals(a, null) && a.Equals(b));
        }

        public static bool operator !=(MoneyAmount a, MoneyAmount b)
        {
            return !(a == b);
        }

        public bool Equals(MoneyAmount other)
        {
            return other != null && this.Amount == other.Amount && this.CurrencySymbol == other.CurrencySymbol;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as MoneyAmount);
        }

        public override int GetHashCode()
        {
            return Amount.GetHashCode() ^ CurrencySymbol.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{1} {2}", this.Amount, this.CurrencySymbol);
        }
        
    }
}
