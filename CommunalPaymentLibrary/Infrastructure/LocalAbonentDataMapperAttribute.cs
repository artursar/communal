﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class LocalAbonentDataMapperAttribute : Attribute
    {
        public string MappingName { get; set; }
        public int ID { get; set; }
        public int ServiceID { get; set; }
        public int ParamID { get; set; }
        public string FieldNameSource { get; set; }
        public string DescriptionArm { get; set; }

        public LocalAbonentDataMapperAttribute()
        {

        }
    }
}
