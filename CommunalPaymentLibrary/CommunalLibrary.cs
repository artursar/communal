﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public class CommunalLibrary
    {
        Settings currentSettings;

        public CommunalLibrary(Settings settings)
        {
            this.currentSettings = settings;
        }

        public IBaseCommunalProvider GetProvider(BodCommunalService serviceType)
        {
            return ProviderLocator.GetProvider(serviceType, currentSettings);
        }
    }
}
