﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CommunalPaymentLibrary
{
    public class DbManager
    {
        private string connectionString;

        public DbManager(DbCredentials credentials)
        {
            if (credentials == null || string.IsNullOrEmpty(credentials.Server) || string.IsNullOrEmpty(credentials.Username) || string.IsNullOrEmpty(credentials.Password))
                throw new TypeInitializationException("Invalid database credentials provided", null);

            connectionString = String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};", credentials.Server, credentials.DbName, credentials.Username, credentials.Password);
        }
        public string ConnectionString { get { return connectionString; } }
        public bool DoTestConnection()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select 1";

                    return Convert.ToInt32(cmd.ExecuteScalar()) == 1;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }


        public DataSet GetLocalAbonents(string abonent, BodCommunalService serviceType)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.GetLocalAbonentData";
                        cmd.Parameters.AddWithValue("@CommunalServiceId", (int)serviceType);
                        cmd.Parameters.AddWithValue("@AbonentNumber", abonent);

                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return ds;
        }
        public DataSet GetLocalAbonentsByPhone(string phone, BodCommunalService serviceType)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.GetLocalAbonentDataByPhone";
                        cmd.Parameters.AddWithValue("@CommunalServiceId", (int)serviceType);
                        cmd.Parameters.AddWithValue("@PhoneNumber", phone);

                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return ds;
        }
        public int CommunalSave(string abonent, BodCommunalService serviceType, int branchCode, MoneyAmount amountToPay, string communalBranch, string abonentName, string providerAbonentId, bool isEvening, PaymentSource source, int? bodAccountID, bool? runCommunalComplete = true, int? remoteDocId = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.CommunalSave";
                        cmd.Parameters.AddWithValue("@BranchCode", branchCode);
                        cmd.Parameters.AddWithValue("@CommunalServiceId", serviceType);
                        cmd.Parameters.AddWithValue("@AmountToPay", amountToPay.Amount);
                        cmd.Parameters.AddWithValue("@IsEvening", isEvening);
                        cmd.Parameters.AddWithValue("@PaymentSource", source);
                        cmd.Parameters.AddWithValue("@ComunalBranch", communalBranch);
                        cmd.Parameters.AddWithValue("@Abonent", abonent);
                        cmd.Parameters.AddWithValue("@ProviderAbonentID", providerAbonentId);
                        cmd.Parameters.AddWithValue("@AbonentName", abonentName);
                        cmd.Parameters.AddWithValue("@BodAccountID", bodAccountID);
                        cmd.Parameters.AddWithValue("@IsComplete", runCommunalComplete);
                        cmd.Parameters.AddWithValue("@RemoteDocID", remoteDocId);

                        SqlParameter payId = new SqlParameter("@PayID", SqlDbType.Int);
                        payId.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(payId);

                        cmd.ExecuteNonQuery();
                        return (int)payId.Value;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void OrangeDetailsSave(int payId, int branchCode, string RRN, string reference)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.OrangeDetailsSave";
                        cmd.Parameters.AddWithValue("@PayID", payId);
                        cmd.Parameters.AddWithValue("@BranchCode", branchCode);
                        cmd.Parameters.AddWithValue("@RRN", RRN);
                        cmd.Parameters.AddWithValue("@Reference", reference);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void UcomDetailsSave(int payId, int branchCode, string RRN, string reference, DateTime payDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.UcomDetailsSave";
                        cmd.Parameters.AddWithValue("@PayID", payId);
                        cmd.Parameters.AddWithValue("@BranchCode", branchCode);
                        cmd.Parameters.AddWithValue("@RRN", RRN);
                        cmd.Parameters.AddWithValue("@Reference", reference);
                        cmd.Parameters.AddWithValue("@PayDate", payDate);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void GncDetailsSave(int payID, int branchCode, string receipt, string reference, DateTime payDate, string payAmount_tv, string payAmount_no_tv)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.GncDetailsSave";
                        cmd.Parameters.AddWithValue("@PayID", payID);
                        cmd.Parameters.AddWithValue("@BranchCode", branchCode);
                        cmd.Parameters.AddWithValue("@Receipt", receipt);
                        cmd.Parameters.AddWithValue("@Reference", reference);
                        cmd.Parameters.AddWithValue("@PayDate", payDate);
                        cmd.Parameters.AddWithValue("@PayAmount_tv", payAmount_tv);
                        cmd.Parameters.AddWithValue("@PayAmount_no_tv", payAmount_no_tv);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void CommunalComplete(int payId, int branchCode, bool isComplete)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "communal.CommunalComplete";
                        cmd.Parameters.AddWithValue("@PayID", payId);
                        cmd.Parameters.AddWithValue("@BranchCode", branchCode);
                        cmd.Parameters.AddWithValue("@IsComplete", isComplete);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }    
}
