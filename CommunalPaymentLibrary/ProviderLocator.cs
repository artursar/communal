﻿using CommunalPaymentLibrary;
using CommunalPaymentLibrary.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    internal class ProviderLocator
    {
        public static IBaseCommunalProvider GetProvider(BodCommunalService serviceType, Settings currentSettings)
        {
            Utilities.DbTest = new DbCredentials(currentSettings.DbSettings.Server, currentSettings.DbSettings.DbName,
                                currentSettings.DbSettings.Username, currentSettings.DbSettings.Password);
            DbManager dm = new DbManager(Utilities.DbTest);

            switch (serviceType)
            {
                case BodCommunalService.None:
                    break;   
                case BodCommunalService.Orange:
                    return new BaseMobilePhoneProvider<OrangeAbonent>(currentSettings);
                case BodCommunalService.VivaCell:
                case BodCommunalService.ElectricityCitizen:
                    return new ElectricityCitizenProvider(dm, currentSettings);
                case BodCommunalService.WaterCitizen:
                    return new WaterCitizenProvider(dm, currentSettings);
                case BodCommunalService.WaterPublic:
                    return new WaterPublicProvider(dm, currentSettings);
                default:
                    break;
            }

            throw new Exception("Not supported service");
        }


        internal static IBaseCommunalProvider GetMobileProvider(BodCommunalService serviceType, Settings currentSettings)
        {
            Utilities.DbTest = new DbCredentials(currentSettings.DbSettings.Server, currentSettings.DbSettings.DbName,
                                currentSettings.DbSettings.Username, currentSettings.DbSettings.Password);
            DbManager dm = new DbManager(Utilities.DbTest);

            switch (serviceType)
            {
                case BodCommunalService.None:
                    break;
                case BodCommunalService.BeelineMobile:
                    return new BeelineMobileProvider(dm, currentSettings);
                //case BodCommunalService.Orange:
                //    return new OrangeProvider(dm, currentSettings);
                //case BodCommunalService.VivaCell:
                //    return new VivacellProvider(dm, currentSettings);
                default:
                    break;
            }

            throw new Exception("Not supported service");
        }


    }
}
