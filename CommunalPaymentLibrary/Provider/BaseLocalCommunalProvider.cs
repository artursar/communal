﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary.Provider
{
    public abstract class BaseLocalCommunalProvider<T> : BaseCommunalProvider<T> where T : BaseLocalAbonent, new()
    {
        protected DbManager dbManager;

        public  virtual List<T> MapLocalAbonent(DataSet ds)
        {
            List<T> abonents = new List<T>();
            try
            {
                string[] cols = ds.Tables[0].Columns.Cast<DataColumn>().Select(s => s.ColumnName).ToArray();

                var props = typeof(T).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                                  .Where(pi => pi.GetCustomAttributesData().Any(d => d.AttributeType == typeof(LocalAbonentDataMapperAttribute))).ToList();

                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    T abonent = new T();

                    foreach (var col in cols)
                    {
                        var prop = props.Where(p => p.GetCustomAttributesData().Any(d => d.NamedArguments.Any(a => a.MemberName == "MappingName" && a.TypedValue.Value.ToString() == col))).Single();

                        if (prop.SetMethod == null || prop.SetMethod.IsPrivate)
                            continue;

                        prop.SetValue(abonent, Convert.ChangeType(item[col], prop.PropertyType));
                    }

                    abonents.Add(abonent);
                }

                return abonents;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual List<T> MapLocalAbonent(DataSet ds, string abonent)
        {
            return this.MapLocalAbonent(ds);
        }

        public override IEnumerable<T> RequestAbonentBill(string abonent)
        {
            try
            {
                DataSet ds = dbManager.GetLocalAbonents(abonent, Services.AsEnumerable().Single());
                if (ds.Tables[0].Rows.Count <= 0)
                    return null;
                return MapLocalAbonent(ds, abonent);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override void PayBill(T abonent, MoneyAmount amount)
        {
            base.PayBill(abonent, amount, null);
        }
        public override void PayBill(T abonent, MoneyAmount amount, int? remoteDocId)
        {
            base.PayBill(abonent, amount, null);
        }

        public override IEnumerable<BaseAbonent> RequestAbonentBillByPhone(string phone)
        {
            try
            {
                DataSet ds = dbManager.GetLocalAbonentsByPhone(phone, Services.AsEnumerable().Single());
                if (ds.Tables[0].Rows.Count <= 0)
                    return null;
                return MapLocalAbonent(ds);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
