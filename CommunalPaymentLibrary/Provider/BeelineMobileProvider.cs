﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary.Provider
{
    public class BeelineMobileProvider : BaseLocalCommunalProvider<BeelineMobileAbonent>
    {
        private BodCommunalService[] services = new[] { BodCommunalService.BeelineMobile };

        public override BodCommunalService[] Services
        {
            get { return services; }
        }

        public BeelineMobileProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override List<BeelineMobileAbonent> MapLocalAbonent(DataSet ds, string abonent)
        {
            if (ds.Tables[0].Rows.Count > 0)
                return base.MapLocalAbonent(ds);

            List<BeelineMobileAbonent> aList = new List<BeelineMobileAbonent>();
            aList.Add(new BeelineMobileAbonent() { AbonentNumber = abonent });

            return aList;
        }

        public override IEnumerable<BeelineMobileAbonent> RequestAbonentBill(string abonent)
        {
            IEnumerable<BeelineMobileAbonent> abs = base.RequestAbonentBill(abonent);
            if (abs == null)
            {
                BeelineMobileAbonent ab = new BeelineMobileAbonent() { AbonentNumber = abonent };
                abs = new List<BeelineMobileAbonent>() { ab };
            }
            return abs;
        }
  
    }
}
