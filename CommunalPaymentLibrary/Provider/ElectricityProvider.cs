﻿using CommunalPaymentLibrary;
using CommunalPaymentLibrary.Provider;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CommunalPaymentLibrary
{
    public class ElectricityCitizenProvider : BaseLocalCommunalProvider<ElectricityCitizen>
    {
        private BodCommunalService[] _services = new[] { BodCommunalService.ElectricityCitizen };

        public ElectricityCitizenProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override BodCommunalService[] Services { get { return _services; } }

    }   

}
