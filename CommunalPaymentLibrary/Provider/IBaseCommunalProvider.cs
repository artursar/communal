﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{

    public interface IBaseCommunalProvider
    {
        BodCommunalService[] Services { get; }
        IEnumerable<BaseAbonent> RequestAbonentBill(string abonent);
        IEnumerable<BaseAbonent> RequestAbonentBillByPhone(string phone);
        void PayBill(BaseAbonent abonent, MoneyAmount amount);
        void PayBill(BaseAbonent abonent, MoneyAmount amount, int? remoteDocId);
    }

}
