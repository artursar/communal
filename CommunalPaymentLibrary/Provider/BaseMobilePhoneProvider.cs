﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary.Provider
{
    public class BaseMobilePhoneProvider<T> : BaseCommunalProvider<T> where T : BaseAbonent, IMobileAbonent, new()
    {
        private BodCommunalService[] services = new[] { BodCommunalService.Orange, BodCommunalService.VivaCell, BodCommunalService.BeelineMobile };

        public override BodCommunalService[] Services { get { return services; } }

        public BaseMobilePhoneProvider(Settings settings)
        {
            this.Settings = settings;
        }

        protected BodCommunalService DetermineMobileProvider(string mobileNumber)
        {
            // Logic goes here
            //return BodCommunalService.BeelineMobile;                            
            return BodCommunalService.Orange;
        }

        protected IBaseCommunalProvider LocateMobileProvider(string abonent)
        {
            BodCommunalService service = DetermineMobileProvider(abonent);
            return ProviderLocator.GetMobileProvider(service, Settings);
        }

        public override IEnumerable<T> RequestAbonentBill(string abonent)
        {
            IBaseCommunalProvider provider = LocateMobileProvider(abonent);
            return (IEnumerable<T>)provider.RequestAbonentBill(abonent);
        }

        public override void PayBill(T abonent, MoneyAmount amount)
        {
            IBaseCommunalProvider provider = LocateMobileProvider(abonent.AbonentNumber);
            provider.PayBill(abonent, amount);
        }

        public override void PayBill(T abonent, MoneyAmount amount, int? remoteDocId)
        {
            IBaseCommunalProvider provider = LocateMobileProvider(abonent.AbonentNumber);
            provider.PayBill(abonent, amount, remoteDocId);
        }
    }
}
