﻿using CommunalPaymentLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary.Provider
{
    public abstract class BaseCommunalProvider<T> : IBaseCommunalProvider where T : BaseAbonent
    {
        public virtual Settings Settings { get; protected set; }

        public abstract BodCommunalService[] Services { get; }
        public virtual IEnumerable<T> RequestAbonentBill(string abonent)
        {
            throw new NotImplementedException();
        }
        public virtual IEnumerable<BaseAbonent> RequestAbonentBillByPhone(string phone)
        {
            throw new NotImplementedException();
        }
        public virtual void PayBill(T abonent, MoneyAmount amount)
        {
            throw new NotImplementedException();
        }
        public virtual void PayBill(T abonent, MoneyAmount amount, int? remoteDocId)
        {
            throw new NotImplementedException();
        }


        #region IBaseCommunalProvider Explicit implementation
        BodCommunalService[] IBaseCommunalProvider.Services
        {
            get { return Services; }
        }
        IEnumerable<BaseAbonent> IBaseCommunalProvider.RequestAbonentBill(string abonent)
        {
            return RequestAbonentBill(abonent);
        }
        IEnumerable<BaseAbonent> IBaseCommunalProvider.RequestAbonentBillByPhone(string phone)
        {
            return RequestAbonentBillByPhone(phone);
        }
        void IBaseCommunalProvider.PayBill(BaseAbonent abonent, MoneyAmount amount)
        {
            T ab = (T)abonent;
            PayBill(ab, amount);
        }
        void IBaseCommunalProvider.PayBill(BaseAbonent abonent, MoneyAmount amount, int? remoteDocId)
        {
            T ab = (T)abonent;
            PayBill(ab, amount, remoteDocId);
        }
        #endregion







    }

}
