﻿using CommunalPaymentLibrary.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunalPaymentLibrary
{
    public class WaterCitizenProvider : BaseLocalCommunalProvider<WaterCitizen>
    {        
        private BodCommunalService[] _services = new[] { BodCommunalService.WaterCitizen };

        public WaterCitizenProvider(DbManager dbManager,  Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override BodCommunalService[] Services { get { return _services; } }

    }

    public class WaterPublicProvider : BaseLocalCommunalProvider<WaterPublic>
    {
        private BodCommunalService[] _services = new[] { BodCommunalService.WaterPublic };

        public WaterPublicProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override BodCommunalService[] Services { get { return _services; } }

    }
}
