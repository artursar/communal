﻿#define TEST
#undef TEST

using AebCommunalLibrary.Abonent;
using BasicLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AebCommunalLibrary.Provider
{
    public class GncProvider : BaseOnlineCommunalProvider<GncAbonent>
    {
        private BodCommunalService[] services = new[] { BodCommunalService.GNC_RosTeleCom };

        public override BodCommunalService[] Services
        {
            get { return services; }
        }


        public GncProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override IEnumerable<GncAbonent> RequestAbonentBill(string abonent)
        {
            try
            {
                AEB.Chain.Providers.ProviderGNC_RosTeleCom provider = new AEB.Chain.Providers.ProviderGNC_RosTeleCom();
                provider.Branch = Settings.Branch.ToString();
                double total;
                double balance_tv;
                double balance_no_tv;
                string clientName;
                int ret = provider.FindAbonentByPhone(false, abonent, out clientName, out total, out balance_tv, out balance_no_tv);
                if (ret != 0)
                {
                    throw new Exception("Բաժանորդը չի գտնվել կամ կապի սխալ");
                }

                GncAbonent gncAbonent = new GncAbonent()
                {
                    Total = (decimal)total,
                    AbonentNumber = abonent,
                    Balance_tv = (decimal)balance_tv,
                    AbonentName = clientName,
                    Balance_no_tv = (decimal)balance_no_tv
                };
                return new List<GncAbonent>() { gncAbonent };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override BasicLibrary.SystemMessages PayBill(GncAbonent abonent, decimal amount)
        {
            return PayBill(abonent, amount, null);
        }
        public override BasicLibrary.SystemMessages PayBill(GncAbonent abonent, decimal amount, int? remoteDocId)
        {
            SystemMessages msgs = new SystemMessages();
            try
            {
                int payId = dbManager.CommunalSave(abonent.AbonentNumber, abonent.CommunalServiceId, Settings.Branch, amount, String.Empty, abonent.AbonentName, String.Empty, Settings.IsEvening, Settings.Source, Settings.BodAccountId, remoteDocId: remoteDocId);
                if (payId > 0)
                {
                    msgs.Add(String.Format("Local Communal Save completed successfully. PayID = {0}", payId), MessageType.Success);

                    // Payment with online provider
                    AEB.Chain.Providers.ProviderGNC_RosTeleCom provider = new AEB.Chain.Providers.ProviderGNC_RosTeleCom();
                    provider.Branch = Settings.Branch.ToString();

                    string authCode;
                    string receipt;
                    DateTime payDate;
                    double amount_tv;
                    double amount_no_tv;
                    int ret;

//#if DEBUG
//                    ret = 0;
//                    authCode = "445566";
//                    receipt = "2522";
//                    payDate = DateTime.Now;
//                    amount_tv = 1716;
//                    amount_no_tv = 10284;
//#else
                    if (Settings.IsLive)
                    {
                        ret = provider.Payment(true, abonent.AbonentNumber, (double)amount, out receipt, out authCode, out payDate, out amount_tv, out amount_no_tv);
                    }
                    else
                    {
                        ret = 0;
                        authCode = "445566";
                        receipt = "2522";
                        payDate = DateTime.Now;
                        amount_tv = 1716;
                        amount_no_tv = 10284;
                    }
//#endif
                    if (ret == 0)
                    {
                        msgs.Add(String.Format("Online payment completed successfully. Return Code = {0}", ret), MessageType.Success);

                        try
                        {
                            dbManager.GncDetailsSave(payId, Settings.Branch, receipt, authCode, payDate, amount_tv.ToString(), amount_no_tv.ToString());

                            msgs.Add("Gnc Details Save executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on DetailsSave step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                        try
                        {
                            dbManager.CommunalComplete(payId, Settings.Branch, !Settings.IsEvening);

                            msgs.Add("Communal Complete executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on CommunalComplete step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                    }
                    else
                    {
                        msgs.Add(String.Format("Online payment failed. Return Code = {0}", ret), MessageType.Error);
                    }
                }
                else
                {
                    msgs.Add(String.Format("Communal Save Failed. A negative PayId received. PayID = {0}", payId), MessageType.Error);
                }
            }
            catch (OnlineCommunalPartialPayedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                msgs.Add(String.Format("An error accured while trying to pay communal. Error received : {0}", ex.Message), MessageType.Error, ex);
            }
            return msgs;
        }
    }
}
