﻿#define TEST
#undef TEST

using AebCommunalLibrary.Abonent;
using BasicLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AebCommunalLibrary.Provider
{
    public class UcomProvider : BaseOnlineCommunalProvider<UcomAbonent>
    {
        private BodCommunalService[] services = new[] { BodCommunalService.UCom };

        public override BodCommunalService[] Services
        {
            get { return services; }
        }

        public UcomProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }

        public override IEnumerable<UcomAbonent> RequestAbonentBill(string abonent)
        {
            try
            {
                AEB.Chain.Providers.UCOM.ProviderUCom provider = new AEB.Chain.Providers.UCOM.ProviderUCom();
                provider.Branch = Settings.Branch.ToString();
                double total;
                ArrayList amountDetails;
                string clientName;
                int ret = provider.GetDebt(true, abonent, out clientName, out total, out amountDetails);
                if (ret != 0)
                {
                    throw new Exception("Բաժանորդը չի գտնվել կամ կապի սխալ");
                }

                UcomAbonent ucomAbonent = new UcomAbonent()
                {
                    Amount = (decimal)total,
                    AbonentNumber = abonent,
                    AbonentName = clientName,
                    Internet = 0,
                    Phone = 0,
                    Tv = 0,
                    Other = 0
                };

                foreach (var item in amountDetails)
                {
                    AEB.Chain.Providers.UCOM.AEB_Param detail = (AEB.Chain.Providers.UCOM.AEB_Param)item;
                    switch (detail.Name)
                    {
                        case "internet":
                            ucomAbonent.Internet = decimal.Parse(detail.Value);
                            break;
                        case "other":
                            ucomAbonent.Other = decimal.Parse(detail.Value);
                            break;
                        case "tv":
                            ucomAbonent.Tv = decimal.Parse(detail.Value);
                            break;
                        case "phone":
                            ucomAbonent.Phone = decimal.Parse(detail.Value);
                            break;
                        default:
                            break;
                    }
                }
                return new List<UcomAbonent>() { ucomAbonent };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SystemMessages PayBill(UcomAbonent abonent, decimal amount)
        {
            return PayBill(abonent, amount, null);
        }

        public override SystemMessages PayBill(UcomAbonent abonent, decimal amount, int? remoteDocId)
        {
            SystemMessages msgs = new SystemMessages();
            try
            {
                int payId = dbManager.CommunalSave(abonent.AbonentNumber, abonent.CommunalServiceId, Settings.Branch, amount, String.Empty, abonent.AbonentName, String.Empty, Settings.IsEvening, Settings.Source, Settings.BodAccountId, remoteDocId: remoteDocId);
                if (payId > 0)
                {
                    msgs.Add(String.Format("Local Communal Save completed successfully. PayID = {0}", payId), MessageType.Success);

                    // Payment with online provider
                    AEB.Chain.Providers.UCOM.ProviderUCom provider = new AEB.Chain.Providers.UCOM.ProviderUCom();
                    provider.Branch = Settings.Branch.ToString();
                    string authCode;
                    string receipt;
                    DateTime payDate;
                    int ret;
//#if DEBUG
//                    authCode = "001122";
//                    receipt = "889900";
//                    payDate = DateTime.Now;
//                    ret = 0;          
//#else
                    if (Settings.IsLive)
                    {
                        ret = provider.Payment(abonent.AbonentNumber, (double)amount, out receipt, out authCode, out payDate);
                    }
                    else
                    {
                        authCode = "001122";
                        receipt = "889900";
                        payDate = DateTime.Now;
                        ret = 0;
                    }
//#endif
                    if (ret == 0)
                    {
                        msgs.Add(String.Format("Online payment completed successfully. Return Code = {0}", ret), MessageType.Success);
                        try
                        {
                            dbManager.UcomDetailsSave(payId, Settings.Branch, receipt, authCode, payDate);

                            msgs.Add("Ucom Details Save executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on DetailsSave step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                        try
                        {
                            dbManager.CommunalComplete(payId, Settings.Branch, !Settings.IsEvening);

                            msgs.Add("Communal Complete executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on CommunalComplete step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                    }
                    else
                    {
                        msgs.Add(String.Format("Online payment failed. Return Code = {0}", ret), MessageType.Error);
                    }
                }
                else
                {
                    msgs.Add(String.Format("Communal Save Failed. A negative PayId received. PayID = {0}", payId), MessageType.Error);
                }
            }
            catch (OnlineCommunalPartialPayedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                msgs.Add(String.Format("An error accured while trying to pay communal. Error received : {0}", ex.Message), MessageType.Error, ex);
            }

            return msgs;
        }

    }
}
