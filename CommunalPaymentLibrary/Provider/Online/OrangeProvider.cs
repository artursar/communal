﻿#define TEST
#undef TEST

using AebCommunalLibrary.Abonent;
using BasicLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AebCommunalLibrary.Provider
{
    public class OrangeProvider : BaseOnlineCommunalProvider<OrangeAbonent>
    {
        private BodCommunalService[] services = new[] { BodCommunalService.Orange };

        public OrangeProvider(DbManager dbManager, Settings settings)
        {
            this.dbManager = dbManager;
            this.Settings = settings;
        }


        public override BodCommunalService[] Services
        {
            get { return services; }
        }

        public override IEnumerable<OrangeAbonent> RequestAbonentBill(string abonent)
        {
            try
            {
                int phone;
                if (!int.TryParse(abonent, out phone))
                {
                    throw new Exception("abonent has wrong format");
                }

                AEB.Chain.Providers.ProviderOrange provider = new AEB.Chain.Providers.ProviderOrange();
                provider.Branch = Settings.Branch.ToString();
                double balance;
                string currency;
                string clientName;
                string account;
                int payMethod;
                int ret = provider.GetPhoneInfo(phone, out balance, out currency, out clientName, out account, out payMethod);
                if (ret != 0)
                {
                    throw new Exception("Բաժանորդը չի գտնվել կամ կապի սխալ");
                }

                OrangeAbonent orangeAbonent = new OrangeAbonent()
                {
                    Amount = (decimal)balance,
                    AbonentNumber = abonent,
                    Account = account,
                    AbonentName = clientName,
                    PayMethod = payMethod
                };
                return new List<OrangeAbonent>() { orangeAbonent };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override BasicLibrary.SystemMessages PayBill(OrangeAbonent abonent, decimal amount)
        {
            return PayBill(abonent, amount, null);
        }
        public override BasicLibrary.SystemMessages PayBill(OrangeAbonent abonent, decimal amount, int? remoteDocId)
        {
            SystemMessages msgs = new SystemMessages();

            int phone;
            if (!int.TryParse(abonent.AbonentNumber, out phone))
            {
                throw new Exception("abonent has wrong format");
            }

            try
            {
                int payId = dbManager.CommunalSave(abonent.AbonentNumber, abonent.CommunalServiceId, Settings.Branch, amount, String.Empty, abonent.AbonentName, String.Empty, Settings.IsEvening, Settings.Source, Settings.BodAccountId, remoteDocId: remoteDocId);
                if (payId > 0)
                {
                    msgs.Add(String.Format("Local Communal Save completed successfully. PayID = {0}", payId), MessageType.Success);
                    // Payment with online provider
                    AEB.Chain.Providers.ProviderOrange provider = new AEB.Chain.Providers.ProviderOrange();
                    provider.Branch = Settings.Branch.ToString();
                    string rrn = String.Empty;
                    int reference = 0;
                    string currency = String.Empty;
                    int ret = 0;

//#if DEBUG
//                    rrn = "08101601260029";
//                    reference = 85734139;
//                    currency = "AMD";
//                    ret = 0;    
//#else
                    if (Settings.IsLive)
                    {
                        ret = provider.Payment_Cur_(phone, (double)amount, currency, out rrn, out reference);
                    }
                    else
                    {
                        rrn = "08101601260029";
                        reference = 85734139;
                        currency = "AMD";
                        ret = 0;
                    }
//#endif
                    if (ret == 0)
                    {
                        msgs.Add(String.Format("Online payment completed successfully. Return Code = {0}", ret), MessageType.Success);

                        try
                        {
                            dbManager.OrangeDetailsSave(payId, Settings.Branch, rrn, reference.ToString());

                            msgs.Add("Ucom Details Save executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on DetailsSave step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                        try
                        {
                            dbManager.CommunalComplete(payId, Settings.Branch, !Settings.IsEvening);

                            msgs.Add("Communal Complete executed successfully", MessageType.Success);
                        }
                        catch (Exception)
                        {
                            throw new OnlineCommunalPartialPayedException("Communal payment failed on CommunalComplete step. PayId =" + payId.ToString()) { PayId = payId };
                        }

                    }
                    else
                    {
                        msgs.Add(String.Format("Online payment failed. Return Code = {0}", ret), MessageType.Error);
                    }
                }
                else
                {
                    msgs.Add(String.Format("Communal Save Failed. A negative PayId received. PayID = {0}", payId), MessageType.Error);
                }
            }
            catch (OnlineCommunalPartialPayedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                msgs.Add(String.Format("An error accured while trying to pay communal. Error received : {0}", ex.Message), MessageType.Error, ex);
            }

            return msgs;

        }
    }
}
